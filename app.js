const http = require('http');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const port = process.env.PORT || 8555;

const map = {
    // Purge docker
    "/clean": "docker system prune -a",
};

const requestHandler = async (request, response) => {
    const url = request.url.replace(/\/$/, "");
    if (map[url]) {
        const command = map[url];

        try {
            await exec(command);
        } catch (e) {
            console.log(e);
            response.end('500');
        }

        response.end('200');
    } else {
        response.end('404');
    }
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});


