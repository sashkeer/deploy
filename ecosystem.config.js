module.exports = {
  apps : [{
    name   : "Remote helper",
    script : "./app.js"
  }],
  deploy: {
    dev: {
      "user" : "root",
      "host" : [process.env.DEV_HOST],
      "ref"  : "origin/main",
      "repo" : "git@gitlab.com:sashkeer/deploy.git",
      "path" : "/var/www/remote-helper"
    },
  },
}
